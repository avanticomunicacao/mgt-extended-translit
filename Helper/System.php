<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Extended Translit
 * @category    Avanti
 * @package     Avanti_ExtendedTranslit
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\ExtendedTranslit\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class System
 */
class System extends AbstractHelper
{
    /**
     * @return mixed
     */
    public function isEnabled() : bool
    {
        return (bool)$this->scopeConfig->getValue(
            Config::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }
}