<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Extended Translit
 * @category    Avanti
 * @package     Avanti_ExtendedTranslit
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\ExtendedTranslit\Model\Filter;

use Avanti\ExtendedTranslit\Api\Filter\TranslitInterface;
use Magento\Framework\Stdlib\StringUtils;

/**
 * Class Translit
 */
class Translit implements TranslitInterface
{

    /**
     * @var
     */
    protected $convertTable;

    /**
     * @var \string[][]
     */
    protected $convertConfig = [
        ['from' => 'Ã', 'to' => 'A'],
        ['from' => 'ã', 'to' => 'a']
    ];

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     */
    public function __construct()
    {
        if ($this->convertConfig) {
            $this->setConvertTable($this->convertConfig);
        }
    }

    /**
     * Get chars convertation table
     *
     * @return array
     */
    public function getConvertTable()
    {
        return $this->convertTable;
    }

    /**
     * Set chars convertation table
     *
     * @return array
     */
    public function setConvertTable($convertConfig)
    {
        foreach ($convertConfig as $configValue) {
            $this->convertTable[(string)$configValue['from']] = (string)$configValue['to'];
        }
    }

    /**
     * Filter value
     *
     * @param string $string
     * @return string
     */
    public function filter($string)
    {
        $string = strtr($string, $this->getConvertTable());
        return '"libiconv"' == ICONV_IMPL ? iconv(
            StringUtils::ICONV_CHARSET,
            'ascii//ignore//translit',
            $string
        ) : $string;
    }
}