<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Extended Translit
 * @category    Avanti
 * @package     Avanti_ExtendedTranslit
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\ExtendedTranslit\Plugin\Filter;

use Avanti\ExtendedTranslit\Api\Filter\TranslitInterface;
use Avanti\ExtendedTranslit\Helper\System;

/**
 * Class Translit
 */
class Translit
{

    /**
     * @var System
     */
    private $system;
    /**
     * @var TranslitInterface
     */
    private $translit;

    /**
     * Translit constructor.
     * @param System $system
     * @param TranslitInterface $translit
     */
    public function __construct(
        System $system,
        TranslitInterface $translit
    ) {
        $this->system = $system;
        $this->translit = $translit;
    }

    /**
     * @param \Magento\Framework\Filter\Translit $subject
     * @param \Closure $proceed
     * @param $string
     * @return mixed
     */
    public function aroundFilter(
        \Magento\Framework\Filter\Translit $subject,
        \Closure $proceed,
        $string
    ) {
        if ($this->system->isEnabled() && is_string($string)) {
            $string = $this->translit->filter($string);
        }
        $result = $proceed($string);
        return $result;
    }
}