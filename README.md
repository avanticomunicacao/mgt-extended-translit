# EXTENDED TRANSLIT - M2 #

Magento 2 module: Add process strings based on convertation table

### Instalation ###

* Composer

Add in 'repositories' of composer.json (magento 2 project):

     "repositories": {
        "mgt-extended-translit": {
            "url": "https://bitbucket.org/avanticomunicacao/mgt-extended-translit.git",
            "type": "git"
        }
     }

Make a require:

    composer require penseavanti/module-extended-translit:dev-master
